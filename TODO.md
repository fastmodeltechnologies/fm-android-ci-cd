# TODO 

### General: 
- Eventually like to create our own system-image that could just be installed and contain all config/dependencies
- Disaster recovery
- Joe: Figure out the VNC issues with current master
- Joe + Matt: Setup EC2
- Get Slack Notification API Key from Sai
- Joe: Move Android SDK location to: “/home/jenkins/Developer/Android”
- Script joining ZeroTier network → Manual Step of approving new devices via Web Interface (as well as setting device name, IP, description, etc.) 
- Update server_script.sh and add `echo -y` to autoconfirm “are you sure?”
- Need to handle credentials for jenkins for the sudo commands in server_script.sh 
- Talk/Rehash about build + test efficiency/optimization, divide and conquer jenkins/CI shit and feature module spikes

### server_script.sh
- Does not currently handle keystore, gradle.properties, emulator/device USB configs
- Hard-coded versions (android-28, build-tools, gradle, etc. etc.)
- Update environment config + use ANDROID_BUILD_TOOLS env. var 
- Install kvm dependencies and add jenkins user to KVM group
- Add zero-tier:
- zerotier-cli join a0cbf4b62aad3818
- Add udev rules:

```
SUBSYSTEM=="usb", ATTR{idVendor}=="22b8", ATTR{idProduct}=="2e81", MODE="0666", GROUP="plugdev"
SUBSYSTEM=="usb", ATTR{idVendor}=="18d1", ATTR{idProduct}=="4ee7", MODE="0666", GROUP="plugdev"
SUBSYSTEM=="usb", ATTR{idVendor}=="2a70", ATTR{idProduct}=="4ee7", MODE="0666", GROUP="plugdev"
SUBSYSTEM=="usb", ATTR{idVendor}=="04e8", ATTR{idProduct}=="6860", MODE="0666", GROUP="plugdev"

cd /etc/udev/rules.d/
sudo chmod a+r 51-android.rules
sudo udevadm control --reload-rules

cd ~/.android
rm adbkey
cd ~/
chmod 710 ~/.android
chown -R $USER:$USER $_
```
 
