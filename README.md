### Build Master/Slave Nodes Configuration:
- OS: Ubuntu 18.04 LTS
- Environment Configuration: 
    - JAVA_HOME=/usr/java/jdk1.8.0_212
    - ANDROID_SDK_ROOT=/home/jenkins/Developer/Android/
    - ANDROID_BUILD_TOOLS=$ANDROID_SDK_ROOT/platform-tools

#### Tools/Utilities:

- Packages installed via APT: 
    - curl, git, vim, zsh,  xfce4, xfce4-goodies
    - TightVNC 1.3.10-0ubuntu4 (https://www.tightvnc.com/)
    - Webmin 1.910 (http://www.webmin.com/)
        - See server_script.sh for adding repository + key
    - OpenSSH (1:7.6p1-4ubuntu0.3)
    - qemu-kvm (2.11.1) 
- Installed via CURL: 
    - ZeroTier: 1.2.12 (https://www.zerotier.com/)
    - SDK Man (https://sdkman.io/) 

#### Android Project Dependencies: 
- Java:
    - Using official JDK8 from Oracle (Version: 1.8.0_212b10)
        
- SDK-Man: 
    - Gradle 5.4.1  
    - Kotlin 1.3.31
    
- Android SDK:
    - tools
    - platform-tools
    - lldb;3.1
    - ndk-bundle
    - platforms;android-28
    - platforms;android-27
    - platforms;android-26
    - build-tools;28.0.3
    - extras;m2repository;com;android;support;constraint;constraint-layout-solver;1.0.2
    - extras;m2repository;com;android;support;constraint;constraint-layout;1.0.2
    - extras;android;m2repository
    - extras;google;m2repository
    - system-images;android-28;google_apis;x86_64
    - system-images;android-27;google_apis;x86
    - system-images;android-26;google_apis;x86


