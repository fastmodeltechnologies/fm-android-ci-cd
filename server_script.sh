#Build Server Configuration

sudo apt update
sudo apt upgrade

sudo apt install curl


##########
### Install SSH:
##########

sudo apt install openssh-server
sudo ufw allow ssh
sudo systemctl enable sshd

##########
### Install VNC Server:
##########

sudo apt install xfce4 xfce4-goodies
sudo apt install tightvncserver

vncserver #will require a VNC password be established

vncserver -kill :1

sudo rm ~/.vnc/xstartup

sudo cp xstartup ~/.vnc/xstartup

sudo chmod +x ~/.vnc/xstartup

sudo cp vncserver.service /etc/systemd/system/vncserver@.service

sudo systemctl daemon-reload

sudo systemctl enable vncserver@1.service

sudo systemctl start vncserver@1

##########
### Install Webmin:
##########

wget -qO- http://www.webmin.com/jcameron-key.asc | sudo apt-key add

sudo add-apt-repository "deb http://download.webmin.com/download/repository sarge contrib"

sudo apt update

sudo apt install webmin

sudo ufw allow from any to any port 10000 proto tcp

##########
### Install git:
##########

sudo apt install git

##########
### Install Java:
##########

sudo mkdir /usr/java
sudo cp jdk-8u212-linux-x64.tar.gz /usr/java/jdk-8u212-linux-x64.tar.gz
sudo tar zxvf /usr/java/jdk-8u212-linux-x64.tar.gz
sudo rm /usr/java/jdk-8u212-linux-x64.tar.gz


##########
### Install sdkman:
##########

curl -s "https://get.sdkman.io" | bash
source "$HOME/.sdkman/bin/sdkman-init.sh"

##########
### Install gradle:
##########

sdk install gradle 5.4.1

##########
### Android SDK:
##########

sudo mkdir ~/Android/sdk
sudo cp sdk-tools-linux-4333796.zip ~/Android/sdk/sdk-tools-linux-4333796.zip
sudo unzip ~/Android/sdk/sdk-tools-linux-4333796.zip
sudo rm ~/Android/sdk/sdk-tools-linux-4333796.zip

~/Android/sdk/tools/bin/sdkmanager --update
~/Android/sdk/tools/bin/sdkmanager --install "tools" "platform-tools" "lldb;3.1" "ndk-bundle" "platforms;android-28" "platforms;android-27" "platforms;android-26" "build-tools;28.0.3" "extras;m2repository;com;android;support;constraint;constraint-layout-solver;1.0.2" "extras;m2repository;com;android;support;constraint;constraint-layout;1.0.2" "extras;android;m2repository" "extras;google;m2repository" 
~/Android/sdk/tools/bin/sdkmanager --install "system-images;android-28;google_apis;x86_64" "system-images;android-27;google_apis;x86" "system-images;android-26;google_apis;x86"

##########
### System Variable Configuration:
##########

export JAVA_HOME=/usr/java/jdk1.8.0_212
export ANDROID_SDK_ROOT=/home/jenkins/Android/sdk

export HOME=$HOME:$ANDROID_SDK_ROOT/platform-tools
